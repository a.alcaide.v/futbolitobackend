package cl.hardroid.futbolito.config

import cl.hardroid.futbolito.entity.request.AccountCredentials
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTLoginFilter : AbstractAuthenticationProcessingFilter {

    constructor(url: String, authenticationManager: AuthenticationManager?) : super(url) {

        this.authenticationManager = authenticationManager
    }

    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {
        val creds = ObjectMapper().readValue(request!!.inputStream, AccountCredentials::class.java)

        return authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        creds.username,
                        creds.password,
                        Collections.emptyList()
                )
        )

    }

    override fun successfulAuthentication(request: HttpServletRequest?,
                                          response: HttpServletResponse?,
                                          chain: FilterChain?,
                                          authResult: Authentication?) {
        TokenAuthenticationService.addAuthentication(response!!, authResult!!.name)
    }
}
