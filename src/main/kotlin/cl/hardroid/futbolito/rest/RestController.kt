package cl.hardroid.futbolito.rest

import cl.hardroid.futbolito.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@EnableAutoConfiguration
@RequestMapping("usuario/")
internal class RestController {

    @Autowired
    lateinit var userRepository: UserRepository

    @GetMapping("validar")
    fun validarUsuario() : String {
        return "Hola Mundo"
    }
}