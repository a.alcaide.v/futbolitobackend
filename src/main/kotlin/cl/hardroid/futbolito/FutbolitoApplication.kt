package cl.hardroid.futbolito

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder




@SpringBootApplication
@EnableAutoConfiguration
class FutbolitoApplication

fun main(args: Array<String>) {
    runApplication<FutbolitoApplication>(*args)
}

@Bean
fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
    return BCryptPasswordEncoder()
}
/*
@Bean
fun dataSource(): DataSource {
    val dataSource = DriverManagerDataSource()
    dataSource.setDriverClassName("com.mysql.jdbc.Driver")
    dataSource.url = "mysql:jdbc://localhost:3306/futbolito"
    dataSource.username = "root"
    dataSource.password = "hardroid"
    return dataSource
}*/
